Presentation on the philosophy of explainable artificial intelligence for "Philosophie der KI" Workshop @ [lab3](https://www.lab3.org), coordinated by [Institut für Philosophie, TU Darmstadt](https://www.philosophie.tu-darmstadt.de/startseite_phil.de.jsp) in cooperation with the [IANUS peace lab](https://www.fif.tu-darmstadt.de/ianus/ianus.de.jsp), 20.02.2020.

Online viewable at [soel.gitlab.io/ki-workshop-ws19](https://soel.gitlab.io/ki-workshop-ws19) or downloadable from this very project page.

Made with [reveal.js](https://github.com/hakimel/reveal.js)!

